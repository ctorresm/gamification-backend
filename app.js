var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var logger = require('morgan');
var indexRouter = require('./routes/routes');
var app = express();


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));


app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method, token');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.use('/api', indexRouter);

module.exports = app;

app.listen(process.env.PORT || 3001, function () {
    console.log('Example app listening on port 3001!');
});

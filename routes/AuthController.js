const AuthController = {};
var db = require('../db/database');

AuthController.login = (req, res) => {
  const user = req.body.user;
  const pass = req.body.pass;
  const url = `
        SELECT * 
        FROM usuario 
        WHERE nick_usuario = "${user}" AND password_usuario = "${pass}" `;
  db.query(url, (error, results, fields) => {
    if (error) throw error;
    if (results.length === 0) {
      return res.status(200).json({
        ok: false,
        user: results,
      });
    } else {
      return res.status(200).json({
        ok: true,
        user: results,
      });
    }
  });
};

AuthController.register = (req, res) => {
  const nick = req.body.nick;
  const user = req.body.nombre;
  const pass = req.body.pass;

  const url = `
        INSERT INTO usuario 
        (nombre_usuario, password_usuario, nick_usuario)
        VALUES ('${user}','${pass}','${nick}')`;
  db.query(url, (error, results) => {
    if (error) {
      return res.status(400).json({
        ok: false,
        error,
      });
    }
    return res.status(200).json({
      ok: true,
    });
  });
};

AuthController.getDataById = (req, res) => {
  const user = req.params.userId;
  const url = `
        SELECT * 
        FROM usuario 
        WHERE id_usuario = "${user}"`;
  db.query(url, (error, results, fields) => {
    if (error) throw error;
    if (results.length === 0) {
      return res.status(200).json({
        ok: false,
        user: results,
      });
    } else {
      return res.status(200).json({
        ok: true,
        user: results,
      });
    }
  });
};

AuthController.getAvatar = (req, res) => {
  const user = req.params.userId;
  const url = `
        SELECT * FROM inventario
        WHERE id_usuario=${user} AND colocado_inventario=1`;
  db.query(url, (error, results, fields) => {
    if (error) throw error;
    if (results.length === 0) {
      return res.status(200).json({
        ok: false,
        user: results,
      });
    } else {
      return res.status(200).json({
        ok: true,
        user: results,
      });
    }
  });
};

AuthController.getItems = (req, res) => {
  const user = req.params.userId;
  const url = `
          SELECT * FROM inventario
          WHERE id_usuario=${user}`;
  db.query(url, (error, results, fields) => {
    if (error) throw error;
    if (results.length === 0) {
      return res.status(200).json({
        ok: false,
        user: results,
      });
    } else {
      const cabezas = results.filter((r) => r.categoria_producto === 'cabeza');
      const cuerpos = results.filter((r) => r.categoria_producto === 'cuerpo');
      const mascotas = results.filter(
        (r) => r.categoria_producto === 'mascota'
      );
      const fondos = results.filter((r) => r.categoria_producto === 'fondo');
      return res.status(200).json({
        ok: true,
        items: {
          cabezas,
          cuerpos,
          mascotas,
          fondos,
        },
      });
    }
  });
};

module.exports = AuthController;

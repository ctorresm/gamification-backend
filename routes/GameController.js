const GameController = {};
var db = require('../db/database');

GameController.listGames = (req, res) => {
  const { userId } = req.params;
  const query = `
        SELECT temporal.* 
        FROM(
            SELECT *,
            rank() 
            OVER (PARTITION BY id_videojuego 
                ORDER BY puntos_ganados 
                DESC) AS puesto FROM ranking) temporal
            WHERE temporal.id_usuario = ${userId}`;
  db.query(query, (error, results, fields) => {
    if (error) throw error;
    if (results.length === 0) {
      return res.status(200).json({
        ok: false,
        games: results,
      });
    } else {
      return res.status(200).json({
        ok: true,
        games: results,
      });
    }
  });
};

GameController.updateGame = (req, res) => {
  const { userId, videojuegoId, exp } = req.body;
  const query1 = `
        UPDATE ranking
        SET puntos_ganados=IF(puntos_ganados>${exp},puntos_ganados,${exp}), completado=1, ultima_horajugada=now()
        WHERE id_usuario=${userId} and id_videojuego=${videojuegoId}`;
  db.query(query1, (error, results) => {
    if (error) {
      throw error;
    }
    if (results.affectedRows) {
      return res.status(200).json({
        ok: true,
      });
    } else {
      return res.status(200).json({
        ok: true,
      });
    }
  });
};

GameController.comprarItem = (req, results) => {
  const { id_usuario, monedas, categoria, id_producto } = req.body;
  const q1 = `
    UPDATE usuario
    SET monedas_usuario = monedas_usuario-${monedas}
    WHERE id_usuario=${id_usuario}`;
  const q2 = `
    UPDATE inventario
    SET colocado_inventario = 0 
    WHERE id_usuario=${id_usuario} AND categoria_producto='${categoria}'`;
  const q3 = `
    INSERT INTO inventario
    VALUES (${id_usuario},'${id_producto}','${categoria}', now(), 1)`;

  db.query(q1, (err, res) => {
    if (err) {
      throw err;
    } else {
      db.query(q2, (err, res) => {
        if (err) {
          throw err;
        } else {
          db.query(q3, (error, res) => {
            if (error) {
              return results.status(200).json({
                ok: false,
                error,
              });
            } else {
              return results.status(200).json({
                ok: true,
              });
            }
          });
        }
      });
    }
  });
};

GameController.updateItem = (req, results) => {
  const { id_usuario, categoria, id_producto } = req.body;
  const q1 = `
    UPDATE inventario
    SET colocado_inventario = 0
    WHERE id_usuario=${id_usuario} AND categoria_producto = '${categoria}';`;
  const q2 = `
    UPDATE inventario
    SET colocado_inventario = 1 
    WHERE id_usuario=${id_usuario} AND categoria_producto='${categoria}' and id_producto='${id_producto}'`;

  db.query(q1, (err, res) => {
    if (err) {
      throw err;
    } else {
      db.query(q2, (error, res) => {
        if (error) {
          return results.status(200).json({
            ok: false,
            error,
          });
        } else {
          return results.status(200).json({
            ok: true,
          });
        }
      });
    }
  });
};

module.exports = GameController;

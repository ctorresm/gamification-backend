const RankingController = {};
var db = require('../db/database');

RankingController.getRankingByGame = (req, res) => {
  const { videojuegoId, userId } = req.params;
  const query = `

        SELECT tem3.*,inven.id_producto,inven.categoria_producto,inven.fecha_inventario,inven.colocado_inventario from (

        SELECT temporal2.*,usuar.nombre_usuario,usuar.nick_usuario,usuar.nivel_usuario from (
        SELECT temporal.* from (
        SELECT *,
            rank() OVER (PARTITION BY id_videojuego ORDER BY puntos_ganados DESC,ultima_horajugada desc) AS puesto
        FROM ranking ) temporal
        where temporal.id_videojuego=${videojuegoId}
        limit 10) temporal2
        LEFT OUTER JOIN (SELECT * FROM usuario) usuar
        ON temporal2.id_usuario=usuar.id_usuario
        union all
        select temporal2.*,usuar.nombre_usuario,usuar.nick_usuario,usuar.nivel_usuario from (
        select temporal.* from (
        SELECT *,
                rank() OVER (PARTITION BY id_videojuego ORDER BY puntos_ganados DESC,ultima_horajugada desc) AS puesto
        FROM ranking ) temporal
        where temporal.id_videojuego=${videojuegoId} and temporal.id_usuario=${userId}
        )temporal2
        left outer join (select * from usuario) usuar
        on temporal2.id_usuario=usuar.id_usuario
        
        ) tem3
        left outer join(
            select * from inventario where colocado_inventario=1) inven
            on
            tem3.id_usuario = inven.id_usuario
        `;

  db.query(query, (error, results, fields) => {
    if (error) throw error;
    console.log(results);
    if (results.length === 0) {
      return res.status(200).json({
        ok: false,
        ranking: results,
      });
    } else {
      return res.status(200).json({
        ok: true,
        ranking: results,
      });
    }
  });
};

module.exports = RankingController;

var express = require('express');
var router = express.Router();

const AuthController = require('./AuthController');
const GameController = require('./GameController');
const RankingController = require('./RankingController');

router.post('/login', AuthController.login);
router.post('/register', AuthController.register);
router.get('/user/:userId', AuthController.getDataById);
router.get('/avatar/:userId', AuthController.getAvatar);
router.get('/items/:userId', AuthController.getItems);

router.get('/games/:userId', GameController.listGames);

router.post('/buy', GameController.comprarItem);
router.put('/updateItem', GameController.updateItem);

router.put('/game', GameController.updateGame);

router.get(
  '/ranking/:videojuegoId/:userId',
  RankingController.getRankingByGame
);

module.exports = router;
